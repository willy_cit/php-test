<?php

/**
 * Convert celsius to fahrenheit.
 *
 * @inheritDoc
 */
class CelsiusToFahrenheit {

  /**
   * Convert graus celsius to fahrenheit.
   *
   * @param float $temperature
   *   Receive a temperatur celsius.
   *
   * @return float
   *   Return a temperature in fahrenheit.
   */
  public static function convertCelsiusToFahrenheit(float $temperature): float {
    //Place your code here
    //(0°C × 9/5) + 32 = 32°F
    return ( $temperature *  9/5) + 32 ;
  }

}
