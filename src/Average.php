<?php

/**
 * Find the average number in a given array.
 *
 * @inheritDoc
 */
class Average {

  /**
   * Find average number in an array.
   *
   * @param array $numbers
   *   Receive the array to calculeta a average number.
   *
   * @return float
   *   Return a value of a average number of the array
   */
  public static function findAverage(array $numbers): float {
    //Place your code here

    $sum = array_sum($numbers);
    $count = count($numbers);

    $avarage = $sum / $count;
    return $avarage;

  }

}
