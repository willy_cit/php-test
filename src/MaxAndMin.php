<?php

/**
 * Find the max and min values in the a given array.
 *
 * @inheritDoc
 */
class MaxAndMin {

  /**
   * Find max a min values in an array.
   *
   * @param array $number
   *   Receive an array to find a min and max values.
   *
   * @return array
   *   Return an array with min and max values of the array
   */
  public static function findMaxAndMin(array $number): array {
    //Place your code here
    return [min($number, max($number))];
  }

}
