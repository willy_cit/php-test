<?php

/**
 * Validade the social number.
 *
 * @inheritDoc
 */
class SocialNumber {

  /**
   * Return a type of a triangle.
   *
   * @param string $socialNumber
   *   Receive a string with the social number do be validated.
   *
   * @return bool
   *   Return a bool with the validation
   */
  public static function validateSocialNumber(string $socialNumber):bool {
    //Place your code here

    /* 
      CPF logic : https://www.youtube.com/watch?v=sIR9c4Qx53U&t=388s
    
     * The first 8 numbers (d1 to d8 )are gaved;
     * 9th digit (d9) is a region information;
     * 10th and 11th are verfication digit
    
    So,
    $tenth = $L 
    where:
    $L = 10 * $d1 + 9 * $d2 + ... + 2 * $d9;
    Then 
    %Lmodule = $L % 11
    $Lmodule === 0 || $Lmodule === 1 ? $L = 0 : $L = 11 - $Lmodule
    
    And then,
    $eleventh = $M  
    $M = 10 * $d2 + 9 * $d3 + ... + 2 * $d10;
    $Mmodule = $M % 11
    $Mmodule === 0 || $Mmodule === 1 ? $M = 0 : $M = 11 - $Mmodule

    */
  $arr_cpf = str_split($socialNumber);
	$L = 0 ;
	$M = 0 ;

  // This module validation is the same for both tests, so i decide to include it as 
  // a function.
	function Validate($module){
		return $module === 0 || $module === 1 
			?  0 
			: 11 - $module;
	}

  // Tests the 9th digit of the array, witch is 10th cpf's digit
	foreach( $arr_cpf as $key => $value){
    if($key <= 8){
      $x = (10 - $key) * $value;
			$L = $L + $x;
		}
	};
  
  // Tests the 10th digit of the array, witch is last cpf's digit
	foreach( $arr_cpf as $key => $value){
    // As explaned, is necessary to start on the d2, so i make it skip the 
    // first item of the array  
		if($key === 0 ){ 
			continue;
		}
		if($key <= 9){
			$y = (11 - $key) * $value;
			$M = $M + $y;
		}
	};

	$tenth = Validate($L % 11);
	$eleventh = Validate($M % 11);
	
  //Final validation 
	return strval($tenth) === $arr_cpf[9] && strval($eleventh) === $arr_cpf[10]
		?  "true"
		:  "false";
}

}
