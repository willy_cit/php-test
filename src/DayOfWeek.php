<?php

/**
 * Return a day of the week of a given interger.
 *
 * @inheritDoc
 */
class DayOfWeek {

  /**
   * Return the day of the week.
   *
   * @param int $number
   *   Receive a interger.
   *
   * @return string
   *   Return a day of the week
   */
  public static function whatDayOfWeek($number) {
    //Place your code here
    $week = [
       1 => "Monday",
       2 => "Tuesday",
       3 => "Wednesday",
       4 => "Thursday",
       5 => "Friday",
       6 => "Saturday",
       7 => "Sunday",
    ];

    return $week[$number];
  }

}
