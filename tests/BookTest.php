<?php

use PHPUnit\Framework\TestCase;

class BookTest extends TestCase {

    public function setUp(): void{
        $this->Person = new Person("John Doe", 37, "Male");
        $this->book = new Book("O Hobbit", "J.R.R Tolkien", 300, $this->Person->getName());
        $this->book2 = new Book("O Senhor dos Aneis - O Retorno do Rei", "J.R.R Tolkien", 527, $this->Person->getName());
    }

    public function testConstructor() 
    {
        $this->assertEquals("O Hobbit", $this->book->getTitle());
        $this->assertEquals("J.R.R Tolkien", $this->book->getAuthor());
        $this->assertEquals(300, $this->book->getTotalPages());
        $this->assertEquals("John Doe", $this->Person->getName());
    }

    public function testOpen()        
    {
        $this->assertEquals(true, $this->book->open());
    }
    public function testClose()
    {
        $this->assertEquals(false, $this->book->close());
    }
    public function testBrowse()
    {
        $this->assertEquals(0, $this->book->browse(301));
        $this->assertEquals(100, $this->book->browse(100));
        $this->assertEquals(0, $this->book->browse(-1));
  
    }
    public function testJumpFoward()
    {
        $this->book->setPage(300);
        $this->assertEquals("You reached the end of the book!",  $this->book->jumpFoward());
        $this->book2->setPage($this->book->getTotalPages()-5);
        $this->assertEquals(($this->book2->getPage()+1),  $this->book2->jumpFoward());
    }
    public function testJumpBackWard()
    {
        $this->book->setPage(1);
        $this->assertEquals("You are in the first page of the book!",  $this->book->jumpBackWard());
        $this->book2->setPage(200);
        $this->assertEquals(($this->book2->getPage()-1), $this->book2->jumpBackWard());
   
    }

}