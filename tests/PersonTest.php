<?php

use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase {

    public function setUp(): void{
        $this->Person = new Person("John Doe", 37, "Male");
    }

    public function testConstructor() 
    {
        // $this->Person = new Person("John Doe", 37, "Male");
        $this->assertEquals("John Doe", $this->Person->getName());
        $this->assertEquals(37, $this->Person->getAge());
        $this->assertEquals("Male", $this->Person->getGenre());
    }

    public function testBirthday(){
        $this->assertEquals(($this->Person->getAge()+1), $this->Person->birthday());
    }
}